function open_page(page) {
  close_modal();
  $(window).scrollTop(0);
  setTimeout(function(){
    $('#app-page').load(page);
  }, 100);
}

function open_last_page() {
  clearTimeout(atualizarChat);
  var lastPage = localStorage.getItem('last-page');
  open_page(lastPage);
}