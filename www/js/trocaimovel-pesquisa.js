function pesquisar(pagina) {  
  $('input', '#formulario-pesquisa').each(function(){
    $(this).val() == "" && $(this).attr('disabled', true);
  });
  $('select', '#formulario-pesquisa').each(function(){
    $(this).val() == "0" && $(this).attr('disabled', true);
  });

  var parametrosPesquisa = $('#formulario-pesquisa').serialize();
  localStorage.setItem('pesquisa', parametrosPesquisa);
  open_page(pagina);
}

function carregar_grid_pesquisa(pagina, paginaAtual) {
  var parametrosPesquisa = localStorage.getItem('pesquisa');
  parametrosPesquisa = parametrosPesquisa+'&pagina='+pagina;

  $.post( 'http://trocaimovel.com.br/api/carregar.total.pesquisa.php', { parametros: parametrosPesquisa }).done(function( data ) {
    var resposta = $.parseJSON(data);
    var total = resposta[0].total;
    var paginas = Math.ceil(total/30);
    
    localStorage.setItem('pesquisa-pagina', pagina);
    localStorage.setItem('pesquisa-pagina-atual', paginaAtual);
    
    $('#total-resultados').html(total);
    /*for(i = 1; i < paginas+1 ; i++){
      var pagina = i * 30;
      $('#paginacao').append('<li id="pagina-'+i+'"><a href="'+urlPaginacao+'?pagina='+pagina+'?atual='+i+'">'+i+'</a></li>');
    }
    $('#pagina-'+atual).addClass('active');*/
    
    $.post( 'http://trocaimovel.com.br/api/carregar.grid.pesquisa.php', { parametros: parametrosPesquisa })
    .done(function( data ) {
      var resposta = $.parseJSON(data);
      if (resposta != null) {
        $.each(resposta, function () {
          var idAnuncio = this['id_anuncio'];
          var template = $('#template').html();
          var titulo = this['categoria'] + ' para ' + this['proposta'];
          var valorImovel = converter_float_real(this['valor_imovel']);
          
          var imagem = '';
          if (this['imagem'].length > 0) imagem = this['imagem'];
          else imagem = 'img/sem-imagem.png';

          if (parseInt(this['numero_quartos']) > 0 ) titulo = this['categoria'] + ' com ' + this['numero_quartos'] + ' quartos para ' + this['proposta'];
          if (this['bairro'].length < 3) this['bairro'] = 'bairro não informado';
          if (this['cidade'].length < 3) this['cidade'] = 'cidade não informada';
          
          template = template.replace(/{{id}}/g, this['id_anuncio']);
          template = template.replace(/{{imagem}}/g, imagem);
          template = template.replace(/{{titulo}}/g, titulo);
          template = template.replace(/{{bairro}}/g, this['bairro']);
          template = template.replace(/{{cidade}}/g, this['cidade']);
          template = template.replace(/{{uf}}/g, this['uf']);
          template = template.replace(/{{descricao}}/g, this['descricao'] + '...');
          template = template.replace(/{{nQuartos}}/g, this['numero_quartos']);
          template = template.replace(/{{nSuites}}/g, this['numero_suites']);
          template = template.replace(/{{nBanheiros}}/g, this['numero_banheiros']);
          template = template.replace(/{{nVagas}}/g, this['numero_vagas']);
          template = template.replace(/{{valor}}/g, valorImovel);
          $('#grid-resultado').append(template);
          if (this['imagem'].length == 0) $('#sem-imagem-'+idAnuncio).show();
        });
      }
      close_modal();
    });
  });
}

function carregar_grid_destaque () {
  $.post( 'http://trocaimovel.com.br/api/carregar.grid.destaque.php')
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $.each(resposta, function () {
        var template = $('#template').html();
        var imagem = '';
        var titulo = this['categoria'] + ' para ' + this['proposta'];
        var valorImovel = converter_float_real(this['valor_imovel']);

        var imagem = '';
        if (this['imagem'].length > 0) imagem = this['imagem'];
         else imagem = 'img/sem-imagem.png';

        if (parseInt(this['numero_quartos']) > 0 ) titulo = this['categoria'] + ' com ' + this['numero_quartos'] + ' quartos para ' + this['proposta'];
        if (this['bairro'].length < 3) this['bairro'] = 'bairro não informado';
        if (this['cidade'].length < 3) this['cidade'] = 'cidade não informada';
        
        template = template.replace(/{{id}}/g, this['id_anuncio']);
        template = template.replace(/{{imagem}}/g, imagem);
        template = template.replace(/{{titulo}}/g, titulo);
        template = template.replace(/{{bairro}}/g, this['bairro']);
        template = template.replace(/{{cidade}}/g, this['cidade']);
        template = template.replace(/{{uf}}/g, this['uf']);
        template = template.replace(/{{descricao}}/g, this['descricao'] + '...');
        template = template.replace(/{{nQuartos}}/g, this['numero_quartos']);
        template = template.replace(/{{nSuites}}/g, this['numero_suites']);
        template = template.replace(/{{nBanheiros}}/g, this['numero_banheiros']);
        template = template.replace(/{{nVagas}}/g, this['numero_vagas']);
        template = template.replace(/{{valor}}/g, valorImovel);
        $('#grid-resultado').append(template);
        if (this['imagem'].length == 0) $('#sem-imagem-'+idAnuncio).show();
      });
    close_modal();
    }
  });
}