function carregar_grid_anuncios_desejo() {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.grid.anuncios.php', {
    token: token
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $.each(resposta, function() {
        var template = $('#template-desejo').html();
        var id = this['id_anuncio'];
        template = template.replace(/{{id}}/g, id);
        template = template.replace(/{{titulo}}/g, this['titulo']);
        $('#list-menu').append(template);
      });
      $('#list-menu').show();
    }
    open_alert_modal('Você não possui nenhum imóvel anunciado para negociar.', 'close_modal();');
  });
}

function exibir_modal_desejo() {
  $('#modal-desejo').modal('show');
}

function adicionar_desejo(idAnuncioProprietario, idAnuncioInteressado) {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/adicionar.desejo.php', {
    token: token,
    idAnuncioProprietario: idAnuncioProprietario,
    idAnuncioInteressado: idAnuncioInteressado
  }).done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;
      if (codigo == 100) open_alert_modal(alerta, "open_page('painel.anuncio.html');");
      else open_alert_modal(alerta, 'close_modal();');
    }
  });
}

function carregar_dados_anuncio(idAnuncio) {
  var token = localStorage.getItem('token');
  var idUsuario = localStorage.getItem('id-usuario');
  
  $.post( 'http://trocaimovel.com.br/api/carregar.dados.anuncio.php', {
    idAnuncio: idAnuncio
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      if (codigo == 100) {
        if (resposta[0].bairro.length < 3) resposta[0].bairro = 'Bairro não informado';
        if (resposta[0].cidade.length < 3) resposta[0].cidade = 'Cidade não informada';
        var localizacao = 'Brasil,' + resposta[0].uf + ',' + resposta[0].cidade + ',' + resposta[0].bairro;

        var idAnunciante = resposta[0].id_usuario;
        localStorage.setItem('chat-id-destinatario',idAnunciante);
        localStorage.setItem('chat-destinatario', idAnunciante);

        $('#id-anunciante').val(idAnunciante);
        $('#titulo').html(resposta[0].titulo);
        $('#anunciante').html(resposta[0].nome + ' ' + resposta[0].sobrenome);
        $('#localizacao').html(resposta[0].cidade + ', ' + resposta[0].bairro);
        $('#descricao').html(resposta[0].descricao);
        $('#proposta').html(resposta[0].proposta);
        $('#bairro').html(resposta[0].bairro);
        $('#n-quartos').html(resposta[0].numero_quartos + ' quartos');
        $('#n-suites').html(resposta[0].numero_suites + ' suítes');
        $('#n-banheiros').html(resposta[0].numero_banheiros + ' banheiros');
        $('#n-vagas').html(resposta[0].numero_vagas + ' vagas');
        $('#metragem').html(resposta[0].metragem + ' m²');
        
        var valorImovel = 'R$ ' +  converter_float_real(resposta[0].valor_imovel);
        var valorCondominio = 'R$ ' +  converter_float_real(resposta[0].valor_condominio);
        var valorIPTU = 'R$ ' + converter_float_real(resposta[0].valor_iptu);
        if(valorImovel == 'R$ 0,00') valorImovel = 'Não informado.';
        if(valorCondominio == 'R$ 0,00') valorCondominio = 'Não informado.';
        if(valorIPTU == 'R$ 0,00') valorIPTU = 'Não informado.';
        $('#valor-imovel').html(valorImovel);
        $('#valor-condominio').html(valorCondominio);
        $('#valor-iptu').html(valorIPTU);

        $('#mapa-cidade').attr('src', 'https://maps.googleapis.com/maps/api/staticmap?center='+localizacao+'&zoom=14&size=400x300');
        $('#mapa-bairro').attr('src', 'https://maps.googleapis.com/maps/api/staticmap?center='+localizacao+'&zoom=15&size=400x300');

        var urlAnuncio = 'http://trocaimovel.com.br/anuncio/'+idAnuncio;
        var compartilhar = resposta[0].categoria + ' para ' + resposta[0].proposta + ' em ' + resposta[0].cidade + '. '+ urlAnuncio;
        if (parseInt(this['numero_quartos']) > 0 ) compartilhar = resposta[0].categoria + ' com ' + resposta[0].numero_quartos + ' quartos para ' + resposta[0].proposta + ' em ' + resposta[0].cidade + '. '+ urlAnuncio;
        $('#compartilhar').val('Veja este anúncio, ' + compartilhar);

        // $('#btn-share-facebook').attr('href', 'http://www.facebook.com/sharer.php?u='+urlAnuncio+'&t='+compartilhar);
        // $('#btn-share-twitter').attr('href', 'https://twitter.com/intent/tweet?text='+compartilhar+' '+urlAnuncio);
        // $('#btn-share-google').attr('href', 'https://plus.google.com/share?url='+urlAnuncio);
        
        if (resposta[0].logotipo.length > 0) {
          $('#img-logo-anunciante').attr('src', 'http://trocaimovel.com.br/files/'+resposta[0].logotipo);
          $('#img-logo-anunciante').show();
          $('#logo-anunciante').show();
        }
        else $('#mapa').addClass('p-top-10');

        if (resposta[0].proposta == "alugar") $('#valor').html('aluguel:');
        if ((resposta[0].exibir_telefone == 1) && (resposta[0].telefone.length > 0)) $('#telefone-numero').html(resposta[0].telefone);
        if (resposta[0].youtube.length > 0) {
          var youtubeURL = resposta[0].youtube;
          $('#li-video').removeClass('display-none-important');
          $('#youtube-iframe').attr('src', 'https://www.youtube.com/embed/'+youtubeURL);
        }
        if ((idAnunciante != idUsuario) && (token != undefined)) {
          $('#btn-chat').show();
          $('#btn-adicionar-desejo').show();
        }
        $('#anuncio').show();
        close_modal();
      }
      else {
        close_modal();
      }
    }
    else {
      close_modal();
    }
  });
}

function compartilhar_anuncio (share) {
  setTimeout(function(){
    $('#btn-share').html('<i class="fa fa-refresh fa-2x fa-spin" aria-hidden="true"></i>');
    var options = {
      message: share,
      subject: 'Anúncio Troca Imóvel'
    }
    var onSuccess = function(result) {
      $('#btn-share').html('<i class="fa fa-share-alt fa-2x" aria-hidden="true"></i>');
      $('#btn-share').css('background', '#003f4f');
    }
    var onError = function(msg) {
      $('#btn-share').html('<i class="fa fa-share-alt fa-2x" aria-hidden="true"></i>');
      open_alert_modal('Erro ao compartilhar.', "close_modal();");
      $('#btn-share').css('background', '#003f4f');
    }
    setTimeout(function(){
      $('#btn-share').html('<i class="fa fa-share-alt fa-2x" aria-hidden="true"></i>');
    }, 2000);
    window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
  }, 100);
}

function carregar_imagens_anuncio(idAnuncio) {  
  $.post( 'http://trocaimovel.com.br/api/carregar.imagens.anuncio.php', {
    idAnuncio: idAnuncio
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $.each(resposta, function () {
        $('.owl-carousel').append('<div class="div-carousel"><img class="lazyOwl" data-src="'+this['imagem']+'"></div>');
      });
      carregar_galeria();
    }
  });
}

function carregar_galeria() {
  $(document).ready(function() {
    $("#owl-carousel").owlCarousel({
      lazyLoad : true
    });
  });
}

function enviar_email_anuncio(idAnuncio) {
  var contatoNome = $('#contato-nome').val();
  var contatoTelefone = $('#contato-telefone').val();
  var contatoEmail = $('#contato-email').val();
  var contatoMensagem = $('#contato-mensagem').val();
  
  $.post( 'http://trocaimovel.com.br/api/enviar.email.contato.php', {
    idAnuncio: idAnuncio,
    contatoNome: contatoNome,
    contatoTelefone: contatoTelefone,
    contatoEmail: contatoEmail,
    contatoMensagem: contatoMensagem
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    var codigo = resposta[0].codigo;
    var alerta = resposta[0].alerta;
    if (codigo == 100) {
      $('#contato-nome').val('');
      $('#contato-telefone').val('');
      $('#contato-email').val('');
      $('#contato-mensagem').val('');
    }
    open_alert_modal(alerta, 'close_modal();')
  });
}

function denunciar_anuncio(idAnuncio, idMotivo) {
  $.post( 'http://trocaimovel.com.br/api/denunciar.imovel.php', {
    idAnuncio: idAnuncio,
    idMotivo: idMotivo
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    var codigo = resposta[0].codigo;
    var alerta = resposta[0].alerta;
    if (codigo == 100) open_alert_modal('Sua denúncia foi registrada com sucesso. Este anúncio será investigado em breve.', "open_page('painel.anuncio.html')");
    else open_alert_modal(alerta, 'close_modal();')
  });
}

function abrir_anuncio(idAnuncio, pagina) {
  localStorage.setItem('id-anuncio', idAnuncio);
  open_page(pagina);
}