var atualizarChat;

function iniciar_chat() {
  var idDestinatario = localStorage.getItem('chat-id-destinatario');
  var destinatario = localStorage.getItem('chat-destinatario');
  $('#chat-destinatario').html(destinatario);
  chat_carregar_conversa(idDestinatario);
  chat_marcar_mensagens();
}

function abrir_conversa(idDestinatario, destinatario) {
  localStorage.setItem('chat-id-destinatario', idDestinatario);
  localStorage.setItem('chat-destinatario', destinatario);
  open_page('painel.chat.html');
}

function chat_enviar_mensagem() {
  var mensagem = $('#chat-mensagem').val();
  var token = localStorage.getItem('token');
  var idDestinatario = localStorage.getItem('chat-id-destinatario');;

  $.post( 'http://trocaimovel.com.br/api/chat.enviar.mensagem.php', {
    token: token,
    idDestinatario: idDestinatario,
    mensagem: mensagem
  }).done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;
      if (codigo == 100) chat_carregar_conversa();
      else open_alert_modal(alerta, 'close_modal();');
      $('#chat-mensagem').val('');
    }
  });
}

function chat_carregar_conversa() {
  var idUsuario = localStorage.getItem('id-usuario');;
  var token = localStorage.getItem('token');
  var idDestinatario = localStorage.getItem('chat-id-destinatario');;

  $.post( 'http://trocaimovel.com.br/api/chat.carregar.conversa.php', {
    token: token,
    idDestinatario: idDestinatario
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $('#chat-conversa').html('');
      $.each(resposta, function() {
        var template;
        var remetente = this['nome_remetente'] + ' ' + this['sobrenome_remetente'];
        var idRemetente = this['id_remetente'];
        
        if (idRemetente == idUsuario) template = $('#template-chat-b').html();
        else template = $('#template-chat-a').html();
        
        template = template.replace(/{{hora}}/g, this['hora']);
        template = template.replace(/{{mensagem}}/g, this['mensagem']);
        $('#chat-conversa').append(template);
      });
      $('#chat-conversa').append('<div class="chat-bottom"></div>');
      $('body').scrollTop($('body')[0].scrollHeight);
    }
  });
}

function chat_marcar_mensagens() {
  var token = localStorage.getItem('token');
  var idDestinatario = localStorage.getItem('chat-id-destinatario');

  $.post( 'http://trocaimovel.com.br/api/chat.marcar.mensagens.php', {
    token: token,
    idDestinatario: idDestinatario
  }).done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      return codigo;
    }
  });
}

function confirmar_remocao_conversa(idCorrespondente) {
  open_confirm_modal('Remover esta conversa?', 'remover_conversa('+idCorrespondente+')');
}

function remover_conversa(idCorrespondente) {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/remover.conversa.php', {
    token: token,
    idCorrespondente: idCorrespondente
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;
      open_alert_modal(alerta, "open_page('painel.gerenciar.mensagens.html');");
    }
    else open_alert_modal('Ocorreu um erro e a conversa não foi removida. Tente mais tarde novamente.', "open_page('painel.gerenciar.mensagens.html');");
  });
}

function carregar_grid_mensagem () {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.grid.mensagens.php', {
    token: token
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var total = 0;
      $.each(resposta, function () {
        var template = $('#template').html();
        var idCorrespondente = this['id_usuario'];
        var acao = this['acao'];
        var totalMensagens = this['nao_lidas'];
        var nome = this['nome'];
        var sobrenome = this['sobrenome'];
        var situacao = this['situacao'];
        var correspondente = nome + ' ' + sobrenome;
        var envelope = '';
        
        if ((acao == 'recebeu') && totalMensagens > 0) envelope = '<i class="fa fa-envelope fa-fw color-salmon" aria-hidden="true"></i> ';
        else envelope = '<i class="fa fa-envelope fa-fw color-midnight" aria-hidden="true"></i> ';

        template = template.replace(/{{envelope}}/g, envelope);
        template = template.replace(/{{correspondente}}/g, correspondente);
        template = template.replace(/{{idCorrespondente}}/g, idCorrespondente);
        if(situacao != 0) {
          total = total + 1;
          $('#list-menu').append(template);
        }
      });
      if (total > 0) $('#list-menu').show();
      else $('#alerta').show();
    }
    else $('#alerta').show();
  });
}
